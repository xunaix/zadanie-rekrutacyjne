import { createSlice } from "@reduxjs/toolkit";
import { specialistsData } from "../data";
import { ISpecialist } from "../components/Specialist";

export interface AppState {
  specialists: ISpecialist[];
  favoriteSpecialistIds: string[];
  searchedText: string;
  selectedTab: "all" | "my";
  myRatings: any[];
}

const initialState: AppState = {
  specialists: specialistsData,
  favoriteSpecialistIds: [],
  searchedText: "",
  selectedTab: "all",
  myRatings: [],
};

export const appSlice = createSlice({
  name: "app",
  initialState,
  reducers: {
    search: (state, action) => {
      state.searchedText = action.payload;
    },
    selectTab: (state, action) => {
      state.selectedTab = action.payload;
    },
    addFavoriteSpecialist: (state, action) => {
      state.favoriteSpecialistIds = [
        ...state.favoriteSpecialistIds,
        action.payload,
      ];
    },
    addMyRatigs: (state, action) => {
      state.myRatings = [action.payload, ...state.myRatings];
    },
  },
});

export const { search, selectTab, addFavoriteSpecialist, addMyRatigs } =
  appSlice.actions;

export default appSlice.reducer;
