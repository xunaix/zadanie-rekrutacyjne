import React, { useEffect, useState } from "react";
import Specialists from "./components/Specialists";
import { useAppSelector } from "./redux/hooks";
import { ISpecialist } from "./components/Specialist";
import Nav from "./components/Nav";

export default function App() {
  const { selectedTab, favoriteSpecialistIds, specialists } = useAppSelector(
    (state) => state.app,
  );

  const [mySpecialists, setMySpecialist] = useState<ISpecialist[]>([]);

  useEffect(() => {
    setMySpecialist(
      specialists.filter((el) => favoriteSpecialistIds.includes(el.id)),
    );
  }, [favoriteSpecialistIds]);

  return (
    <div style={{ margin: "0 auto", maxWidth: 1385, padding: "64px 20px" }}>
      <Nav />
      {selectedTab === "all" ? (
        <Specialists key="all" specialists={specialists} />
      ) : (
        <Specialists key="my" specialists={mySpecialists} />
      )}
    </div>
  );
}
