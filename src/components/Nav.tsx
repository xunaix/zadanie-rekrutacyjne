import React from "react";
import Search from "./Search";
import Tabs from "./Tabs";
import Counter from "./Counter";

export default function Nav() {
  return (
    <nav
      style={{
        display: "flex",
        alignItems: "center",
        marginBottom: 30,
        justifyContent: "space-between",
      }}
    >
      <Counter />
      <Tabs />
      <Search />
    </nav>
  );
}
