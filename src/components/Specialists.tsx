import React, { useEffect, useState } from "react";
import Specialist, { ISpecialist } from "./Specialist";
import ReactPaginate from "react-paginate";
import styled from "styled-components";
import { useAppSelector } from "../redux/hooks";

interface ISpecialists {
  specialists: ISpecialist[];
}

export default function Specialists({ specialists }: ISpecialists) {
  const { searchedText } = useAppSelector((state) => state.app);
  const itemsPerPage = 20;

  const [itemOffset, setItemOffset] = useState(0);

  const [filteredSpecialist, setFilteredSpecialist] = useState(specialists);

  useEffect(() => {
    setFilteredSpecialist(
      specialists.filter((specialist) =>
        specialist.name.toLowerCase().includes(searchedText?.toLowerCase()),
      ),
    );
  }, [searchedText]);

  const endOffset = itemOffset + itemsPerPage;
  const currentItems = filteredSpecialist.slice(itemOffset, endOffset);
  const pageCount = Math.ceil(filteredSpecialist.length / itemsPerPage);

  const handlePageClick = (event: { selected: number }) => {
    const newOffset =
      (event.selected * itemsPerPage) % filteredSpecialist.length;

    setItemOffset(newOffset);
  };

  const Wrapper = styled.div`
    display: flex;
    flex-wrap: wrap;
    justify-content: space-around;
    row-gap: 30px;
    margin: 0 auto;
  `;

  const PaginationWrapper = styled.div`
    display: flex;
    justify-content: center;
    margin-top: 40px;
  `;

  return (
    <div>
      <Wrapper>
        {currentItems.map((el) => (
          <Specialist key={el.id} {...el} />
        ))}
      </Wrapper>

      <PaginationWrapper>
        <ReactPaginate
          breakLabel="..."
          nextLabel="next >"
          onPageChange={handlePageClick}
          pageRangeDisplayed={5}
          pageCount={pageCount}
          previousLabel="< previous"
          renderOnZeroPageCount={null}
          containerClassName="pagination"
          activeClassName="active"
          pageClassName="page-item"
          pageLinkClassName="page-link"
          previousClassName="page-item"
          previousLinkClassName="page-link"
          nextClassName="page-item"
          nextLinkClassName="page-link"
          breakClassName="page-item"
          breakLinkClassName="page-link"
        />
      </PaginationWrapper>
    </div>
  );
}
