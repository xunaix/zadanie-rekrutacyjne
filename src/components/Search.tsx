import React, { useState } from "react";
import { useAppDispatch, useAppSelector } from "../redux/hooks";
import { search } from "../redux/slice";
import { SearchIcon } from "./icons";
import styled from "styled-components";

const Wrapper = styled.div`
  background: transparent;
  width: 316px;
  display: flex;
  padding: 15px 10px 15px 10px;
  gap: 8px;

  &:hover {
    background: #fff;
  }
`;

const Input = styled.input`
  border: 0;
  background: transparent;
  font-size: 14px;
`;

export default function Search() {
  const { searchedText } = useAppSelector((state) => state.app);
  const dispatch = useAppDispatch();

  const handleSearch = (e: React.ChangeEvent<HTMLInputElement>) => {
    dispatch(search(e.target.value));
  };

  return (
    <Wrapper>
      <SearchIcon />
      <Input
        key="search"
        type="text"
        placeholder="Search..."
        onChange={handleSearch}
        value={searchedText}
      />
    </Wrapper>
  );
}
