import React, { useEffect, useState } from "react";
import styled from "styled-components";
import { useAppDispatch, useAppSelector } from "../redux/hooks";
import { addFavoriteSpecialist, addMyRatigs } from "../redux/slice";
import {
  GreyStarIcon,
  BlueStarIcon,
  MoreIcon,
  HeartIcon,
  BellIcon,
  CalendarIcon,
  MailIcon,
} from "./icons";

export interface ISpecialist {
  id: string;
  name: string;
  avatar: string;
  rating: Array<number>;
  job: string;
}

const Img = styled.img`
  width: 100px;
  height: 100px;
  border-radius: 50%;
  margin: 0 auto;
  display: block;
  margin-bottom: 24px;
`;

const Wrapper = styled.div`
  background-color: #fff;
  border: 1px solid #fff;
  padding-top: 13px;
  width: 323px;
  border-radius: 4px;
  box-shadow: 0px 3px 16px 0px rgba(231, 234, 247, 0.35);

  &:hover {
    border: 1px solid #3540ff;
    box-shadow: 0px 3px 16px 0px rgba(231, 234, 247, 0.35);

  }
`;

const Name = styled.p`
  font-weight: 600;
  font-size: 19;
  line-height: 26px;
  text-align: center;
  color: #000;
`;

const Job = styled.p`
  font-weight: 400;
  font-size: 14px;
  text-align: center;
  color: #a2a8c1;
  margin-bottom: 40px;
`;

const Stars = styled.div`
  display: flex;
  gap: 8px;
`;

const RatingWrapper = styled.div`
  display: flex;
  height: 88px;
  align-items: center;
  gap: 42px;
  padding: 0 33px;
`;

const OptionButton = styled.button`
  color: #c5cdda;
  border: 0;
  background-color: #fff;
  border-radius: 4px;
  width: 40px;
  height: 32px;

  &:hover {
    color: #3540ff;
    background-color: #f3f5fc;
    cursor: pointer;
  }
`;

const HeartButton = styled.button`
  border: 0;
  background-color: #fff;
  padding: 0;
  margin: 0;
  width: 20px;
  height: 18px;
  position: relative;
`;

const ButtonsWrapper = styled.div`
  display: flex;
  justify-content: space-between;
  padding: 0 20px 0 10px;
`;

const Separator = styled.div`
  height: 1px;
  background: rgba(242, 244, 246, 1);
`;

const IconsWrapper = styled.div`
  display: flex;
  gap: 8px;
`;

const IconButton = styled.button`
  color: #c5cdda;
  background: transparent;
  border: 3px solid transparent;
  width: 100%;
  height: 44px;
  position: relative;

  &::after {
    display: block;
    background-color: transparent;
    content: "";
    width: 100%;
    position: absolute;
    height: 3px;
    bottom: 0;
  }
  &:hover {
    color: #3540ff;
    cursor: pointer;

    &::after {
      background-color: #3540ff;
    }
  }
`;

const StarButton = styled.button`
  height: 28px;
  width: 28px;
  border: 0;
  background-color: transparent;
  color: #c5cdda;

  &:hover {
    cursor: pointer;
  }
`;

const RatingAv = styled.p`
  font-weight: 600;
  font-size: 30px;
  line-height: 41px;
  text-align: center;
`;

const RatingSum = styled.p`
  font-weight: 400;
  font-size: 12px;
  line-height: 16px;
  text-align: center;
  color: #a2a8c1;
`;

const ProfilButtonsWrapper = styled.div`
  display: flex;
`

const ProfilButton = styled.button`
  height: 59px;
  font-size: 12px;
  font-weight: 400;
  display: flex;
  align-items: center;
  justify-content: center;
  color: rgba(162, 168, 193, 1);
  width: 100%;
  border: 0;
  border-top: 1px solid rgba(242, 244, 246, 1);
  text-transform: uppercase;
  background-color: transparent;

&:first-child {
  border-right: 1px solid rgba(242, 244, 246, 1);
}

  &:hover {
    background-color: rgba(53, 64, 255, 1);
color: #fff;
  }

`

export default function Specialist({
  avatar,
  id,
  name,
  rating,
  job,
}: ISpecialist) {
  const dispatch = useAppDispatch();
  const {favoriteSpecialistIds, myRatings} = useAppSelector(state => state.app)



  const myRating = myRatings.find(el => el.id === id)
  
  

  const [updateRatings, setUpdateRatings] = useState(!!myRating ? myRating.rating : rating)

  const handleAddToFavorite = (id: string) => {
    dispatch(addFavoriteSpecialist(id));
  };

  const [ratingAv, setRatingAv] = useState("")

  useEffect(() => {
    setRatingAv((
      updateRatings.reduce((acc : number, curr: number) => acc + curr, 0) / updateRatings.length
    ).toFixed(1))
  }, [updateRatings])

 
  const handleRating = (idx: number) => {
    if (!myRating) {
      dispatch(addMyRatigs({id, rating: [...updateRatings, idx+1]}))
      setUpdateRatings([...updateRatings, idx+1])
    } else {
      const temp = updateRatings.slice(0, -1)
      dispatch(addMyRatigs({id, rating: [...temp, idx+1]}))
    }
    
    
  }
  

  return (
    <Wrapper>
      <ButtonsWrapper>
        <OptionButton>
          <MoreIcon />
        </OptionButton>
        <HeartButton onClick={() => handleAddToFavorite(id)}>
          <HeartIcon color={favoriteSpecialistIds.includes(id) ? "#3540FF": '#c5cdda'} />
        </HeartButton>
      </ButtonsWrapper>
      <Img src={avatar} alt={`image of ${name}`} />
      <Name>{name}</Name>
      <Job>{job}</Job>

      <IconsWrapper>
        <IconButton>
          <BellIcon />
        </IconButton>
        <IconButton>
          <CalendarIcon />
        </IconButton>
        <IconButton>
          <MailIcon />
        </IconButton>
      </IconsWrapper>

      <Separator />

      <RatingWrapper>
        <Stars>
          {Array.from(Array(5)).map((_, idx) => {
            return (
              <StarButton
              key={idx}
                onClick={() => handleRating(idx)}
              >
                {!!myRating && myRating.rating[myRating.rating.length-1] >= idx + 1 ? <BlueStarIcon /> : <GreyStarIcon />}
              </StarButton>
            );
          })}
        </Stars>
        <div>
          <RatingAv>{ratingAv.replace(".", ",")}</RatingAv>
          <RatingSum>({updateRatings.length})</RatingSum>
        </div>
      </RatingWrapper>
      <ProfilButtonsWrapper>
        <ProfilButton>profil</ProfilButton>
        <ProfilButton>book a visit</ProfilButton>
      </ProfilButtonsWrapper>
    </Wrapper>
  );
}
