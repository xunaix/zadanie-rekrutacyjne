import React from "react";
import styled from "styled-components";
import { useAppSelector } from "../redux/hooks";

const Wrapper = styled.div`
  font-weight: 600;
  font-size: 30px;
  line-height: 41px;
`;

export default function Counter() {
  const { favoriteSpecialistIds } = useAppSelector((state) => state.app);
  return (
    <Wrapper>Favorite specialists ({favoriteSpecialistIds.length})</Wrapper>
  );
}
