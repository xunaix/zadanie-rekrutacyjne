import React from "react";
import { useAppDispatch, useAppSelector } from "../redux/hooks";
import { selectTab } from "../redux/slice";
import styled from "styled-components";

const Button = styled.button<{ active: boolean }>`
  font-weight: ${({ active }) => (active ? "600" : "400")};
  background-color: ${({ active }) => (active ? "#3540FF" : "#E2E4EC")};
  color: ${({ active }) => (active ? "#fff" : "#A2A8C1")};
  box-shadow: ${({ active }) =>
    active ? "0px 10px 16px 0px rgba(203, 212, 250, 0.7)" : "0"};
  height: 50px;
  width: 200px;
  border: 0;
  cursor: pointer;

  &:first-child {
    border-radius: 4px 0px 0px 4px;
  }

  &:last-child {
    border-radius: 0px 4px 4px 0px;
  }

  &:hover {
    color: ${({ active }) => (active ? "#fff" : "#3540FF")};
  }
`;

export default function Tabs() {
  const dispatch = useAppDispatch();
  const { selectedTab } = useAppSelector((state) => state.app);

  const handleClick = (tab: "all" | "my") => {
    dispatch(selectTab(tab));
  };

  return (
    <div>
      <Button active={selectedTab === "all"} onClick={() => handleClick("all")}>
        All favorite
      </Button>
      <Button active={selectedTab === "my"} onClick={() => handleClick("my")}>
        My specialists
      </Button>
    </div>
  );
}
